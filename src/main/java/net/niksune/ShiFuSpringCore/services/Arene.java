package net.niksune.ShiFuSpringCore.services;

import net.niksune.ShiFuSpringCore.enums.Tirage;
import net.niksune.ShiFuSpringCore.beans.ShiFuMiste;
import net.niksune.ShiFuSpringCore.beans.Victoire;

public class Arene {

    public static Victoire combat(ShiFuMiste joueur1, ShiFuMiste joueur2, int nbManchesGagnantes) {
        int scoreJoueur1 = 0, scoreJoueur2 = 0;
        Tirage tirageJoueur1, tirageJoueur2;

        while (scoreJoueur1 != nbManchesGagnantes && scoreJoueur2 != nbManchesGagnantes) {
            tirageJoueur1 = joueur1.tirage();
            tirageJoueur2 = joueur2.tirage();
            if (vainqueurTirage(tirageJoueur1, tirageJoueur2) == 1)
                scoreJoueur1++;
            else if (vainqueurTirage(tirageJoueur1, tirageJoueur2) == 2)
                scoreJoueur2++;
        }

        if (scoreJoueur1 == nbManchesGagnantes)
            return (new Victoire(nbManchesGagnantes, scoreJoueur2, joueur1, joueur2));
        else
            return (new Victoire(nbManchesGagnantes, scoreJoueur1, joueur2, joueur1));

    }

    /**
     * Retourne le vainqueur d'un tirage de ShiFuMi
     *
     * @param tirageJoueur1 net.niksune.ShiFuSpringCore.enums.Tirage du joueur1
     * @param tirageJoueur2 net.niksune.ShiFuSpringCore.enums.Tirage du joueur2
     * @return 0 si égalité, 1 si joueur1 vainqueur, 2 si joueur2 vainqueur
     */
    public static int vainqueurTirage(Tirage tirageJoueur1, Tirage tirageJoueur2) {
        if (tirageJoueur1 == tirageJoueur2)
            return 0;
        else if ((tirageJoueur1 == Tirage.PIERRE && tirageJoueur2 == Tirage.CISEAU)
                || (tirageJoueur1 == Tirage.FEUILLE && tirageJoueur2 == Tirage.PIERRE)
                || (tirageJoueur1 == Tirage.CISEAU && tirageJoueur2 == Tirage.FEUILLE))
            return 1;
        else
            return 2;
    }

}
