package net.niksune.ShiFuSpringCore.beans;

import net.niksune.ShiFuSpringCore.beans.ShiFuMiste;

public class Victoire {

    private int nbManchesGagnantes;
    private int nbManchesVaincu;
    private ShiFuMiste gagnant;
    private ShiFuMiste vaincu;

    public Victoire(int nbManchesGagnantes, int nbManchesVaincu, ShiFuMiste gagnant, ShiFuMiste vaincu) {
        this.nbManchesGagnantes = nbManchesGagnantes;
        this.nbManchesVaincu = nbManchesVaincu;
        this.gagnant = gagnant;
        this.vaincu = vaincu;
    }

    @Override
    public String toString() {
        return "net.niksune.ShiFuSpringCore.beans.Victoire{" +
                "nbManchesGagnantes=" + nbManchesGagnantes +
                ", nbManchesVaincu=" + nbManchesVaincu +
                ", gagnant=" + gagnant.getNom() +
                ", vaincu=" + vaincu.getNom() +
                '}';
    }

    public int getNbManchesGagnantes() {
        return nbManchesGagnantes;
    }

    public void setNbManchesGagnantes(int nbManchesGagnantes) {
        this.nbManchesGagnantes = nbManchesGagnantes;
    }

    public int getNbManchesVaincu() {
        return nbManchesVaincu;
    }

    public void setNbManchesVaincu(int nbManchesVaincu) {
        this.nbManchesVaincu = nbManchesVaincu;
    }

    public ShiFuMiste getGagnant() {
        return gagnant;
    }

    public void setGagnant(ShiFuMiste gagnant) {
        this.gagnant = gagnant;
    }

    public ShiFuMiste getVaincu() {
        return vaincu;
    }

    public void setVaincu(ShiFuMiste vaincu) {
        this.vaincu = vaincu;
    }
}
