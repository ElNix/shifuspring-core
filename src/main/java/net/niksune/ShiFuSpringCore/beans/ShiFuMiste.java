package net.niksune.ShiFuSpringCore.beans;

import net.niksune.ShiFuSpringCore.exceptions.NotHundredException;
import net.niksune.ShiFuSpringCore.enums.Tirage;

import java.util.Random;

public class ShiFuMiste {

    private int ID = 0;
    private String nom;
    private int pierre;
    private int feuille;
    private int ciseau;

    public ShiFuMiste() {
    }

    public ShiFuMiste(String nom, int pierre, int feuille, int ciseau) {

        if ((pierre + feuille + ciseau) != 100)
            throw new NotHundredException();

        this.nom = nom;
        this.pierre = pierre;
        this.feuille = feuille;
        this.ciseau = ciseau;
    }

    public ShiFuMiste(int ID, String nom, int pierre, int feuille, int ciseau) {
        this.ID = ID;
        this.nom = nom;
        this.pierre = pierre;
        this.feuille = feuille;
        this.ciseau = ciseau;
    }

    public Tirage tirage() {
        Random r = new Random();
        int tirageInt = r.nextInt(100);

        if (tirageInt < this.pierre)
            return Tirage.PIERRE;
        else if (tirageInt < (this.pierre + this.feuille))
            return Tirage.FEUILLE;
        else
            return Tirage.CISEAU;
    }

    @Override
    public String toString() {
        return "ShiFuMiste{" +
                "ID=" + ID +
                ", nom='" + nom + '\'' +
                ", pierre=" + pierre +
                ", feuille=" + feuille +
                ", ciseau=" + ciseau +
                '}';
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getPierre() {
        return pierre;
    }

    public void setPierre(int pierre) {
        this.pierre = pierre;
    }

    public int getFeuille() {
        return feuille;
    }

    public void setFeuille(int feuille) {
        this.feuille = feuille;
    }

    public int getCiseau() {
        return ciseau;
    }

    public void setCiseau(int ciseau) {
        this.ciseau = ciseau;
    }

    public int getID() { return ID; }

    public void setID(int ID) { this.ID = ID; }

}
