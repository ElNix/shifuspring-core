package net.niksune.ShiFuSpringCore.exceptions;

public class NotHundredException extends RuntimeException {
    public NotHundredException() {
        super("Le Shifumeur doit avoir un total de 100 entre pierre, feuille et ciseau");
    }
}