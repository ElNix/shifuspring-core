package net.niksune.ShiFuSpringCore;

import net.niksune.ShiFuSpringCore.beans.ShiFuMiste;
import net.niksune.ShiFuSpringCore.services.Arene;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass {

    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:/spring-context.xml");

        ShiFuMiste rocky = context.getBean("rock", ShiFuMiste.class);
        ShiFuMiste edouard = new ShiFuMiste("Edouard aux Mains d'Argent", 15, 10, 75);
        ShiFuMiste konan = new ShiFuMiste("Konan de Ame", 10, 75, 15);

        for (int i = 0; i < 5; i++) {
            System.out.println(Arene.combat(rocky, edouard, 3));
            System.out.println(Arene.combat(edouard, konan, 3));
            System.out.println(Arene.combat(konan, rocky, 3));
        }

    }

}
