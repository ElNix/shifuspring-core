package net.niksune.ShiFuSpringCore;

import net.niksune.ShiFuSpringCore.beans.ShiFuMiste;
import net.niksune.ShiFuSpringCore.beans.Victoire;
import net.niksune.ShiFuSpringCore.enums.Tirage;
import net.niksune.ShiFuSpringCore.services.Arene;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AreneTest {

    @Test
    void victoireTest() {

        ShiFuMiste rocky = new ShiFuMiste("Rocky Balboa", 100, 0, 0);
        ShiFuMiste edouard = new ShiFuMiste("Edouard aux Mains d'Argent", 0, 0, 100);
        ShiFuMiste konan = new ShiFuMiste("Konan de Ame", 0, 100, 0);

        assertEquals((new Victoire(3,0, rocky, edouard)).toString(),
                Arene.combat(rocky, edouard, 3).toString());
        assertEquals((new Victoire(3,0, edouard, konan)).toString(),
                Arene.combat(konan, edouard, 3).toString());
        assertEquals((new Victoire(3,0, konan, rocky)).toString(),
                Arene.combat(rocky, konan, 3).toString());

    }

    @org.junit.jupiter.api.Test
    void vainqueurTirage() {
        assertEquals(0,Arene.vainqueurTirage(Tirage.PIERRE,Tirage.PIERRE));
        assertEquals(0,Arene.vainqueurTirage(Tirage.CISEAU,Tirage.CISEAU));
        assertEquals(0,Arene.vainqueurTirage(Tirage.FEUILLE,Tirage.FEUILLE));
        assertEquals(1,Arene.vainqueurTirage(Tirage.PIERRE,Tirage.CISEAU));
        assertEquals(1,Arene.vainqueurTirage(Tirage.FEUILLE,Tirage.PIERRE));
        assertEquals(1,Arene.vainqueurTirage(Tirage.CISEAU, Tirage.FEUILLE));
        assertEquals(2,Arene.vainqueurTirage(Tirage.PIERRE,Tirage.FEUILLE));
        assertEquals(2,Arene.vainqueurTirage(Tirage.FEUILLE,Tirage.CISEAU));
        assertEquals(2, Arene.vainqueurTirage(Tirage.CISEAU,Tirage.PIERRE));
    }
}